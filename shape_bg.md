Andriod中的shape也是日常工作用的比较多的组件,一般用来设置为其他组件的背景,这里我来简单介绍下它的使用方法:
首先看下shape的几个子属性

下面来详解下几个比较常用的属性
a). <corners/> 即设置shape的圆角,它的几个属性为:
android:radius="5dp"  整个边角的角度
android:topLeftRadius="20dp" 左上角角度
 android:topRightRadius="20dp" 右上角角度
 android:bottomLeftRadius="20dp"   左下角角度
android:bottomRightRadius="20dp"  右下角角度

b).<solid/> shape的填充色,只有一个属性
android:color="" 来设置shape的颜色

c).<padding/> 设置shape的边距
四个属性 left top bottom right

d).<stroke/> 设置shape的描边
android:width=""   描边的宽度
android:color=""    描边的颜色
下面这两个属性,是设置shape的描边为嘘线的方法
android:dashWidth="5dp"    这个是指虚线实心的宽度
android:dashGap="3dp"      这个是指 虚线实心的间距

